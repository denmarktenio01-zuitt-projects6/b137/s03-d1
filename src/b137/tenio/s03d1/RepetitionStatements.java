package b137.tenio.s03d1;

public class RepetitionStatements {
    public static void main(String[] args) {


        // for statement
        for (int i = 1; i <= 5; i++) {
            System.out.println(i);
        }

        // Mini activity
        // Declare an array of numbers 100, 200, 300, 400, 500
        // Use for statement to display those numbers in the console

        int[] numbers = {100, 200, 300, 400, 500};

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        System.out.println();

        for (int number : numbers) {
            System.out.println(number);
        }



        System.out.println();

        String[] lords = {"Nito", "Bed of Chaos", "4 Kings", "Gwyn"};

        for (String lord : lords) {
            System.out.println(lord);
        }

        // while and do-while statements
        System.out.println();
        int x = 0;
        int y = 10;

        while (x <= 10) {
            System.out.println("Loop number: " + x);
            x++;
        }

        System.out.println();

        do {
            System.out.println("Countdown: " + y);
            y--;
        } while (y >= 0);
    }
}
